module Lexeme where
import           Prettyprinter                  ( Doc
                                                , pretty
                                                , brackets
                                                , line
                                                , parens
                                                , (<+>)
                                                )
import           Data.Text                      ( Text )
_genModule :: Doc Text
_genModule = "module"

_genBegin :: Doc Text
_genBegin = "begin"

_genEnd :: Doc Text
_genEnd = "end"

_genIn :: Doc Text
_genIn = "i_IN"

_genOut :: Doc Text
_genOut = "o_OUT"

_genClk :: Doc Text
_genClk = "i_CLK"

_genReg :: Doc Text
_genReg = "reg"

_genRatioCntr :: Doc Text
_genRatioCntr = "r_RATIO_CNTR"

_genCombEnable :: Doc Text
_genCombEnable = "r_COMB_EN"

_genIntReg :: Doc Text
_genIntReg = "r_INT_S"

_genCombReg :: Doc Text
_genCombReg = "r_COMB_S"

_genDelayReg :: Doc Text
_genDelayReg = "r_DELAY_S"

_genArrow :: Doc Text
_genArrow = "<="

_genPlus :: Doc Text
_genPlus = "+"

_genMinus :: Doc Text
_genMinus = "-"

_genSigned :: Doc Text
_genSigned = "signed"

_genElse :: Doc Text
_genElse = "else"

_genBeginBlock :: Doc Text -> Doc Text
_genBeginBlock x = _genBegin <> _genLinePad x <> _genEnd

_genLinePad :: Doc Text -> Doc Text
_genLinePad x = line <> x <> line

_genIf :: Doc Text -> Doc Text -> Doc Text -> Doc Text
_genIf cond body body2 =
  "if"
    <+> parens cond
    <+> _genBeginBlock body
    <>  _genLinePad _genElse
    <>  _genBeginBlock body2

_genWidthVerilog :: Int -> Doc Text
_genWidthVerilog x = brackets $ pretty x <> ":0"

