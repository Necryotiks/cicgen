-- |
-- Module      : Generate
-- Description : Contains functions to generate HDL
-- Copyright   : (c) Elliott Villars, 2020
-- License     : GPL-3
-- Maintainer  : elliottvillars@gmail.com
-- Stability   : experimental
-- Portability : POSIX
--
module Generate
  ( generateVerilog
  )
where
import GenInternal
import Lexeme(_genLinePad)
import Cicgen(Positive,fromPositive,PEP2,fromPEP2)
import Data.Text(Text)
import Prettyprinter
import Prettyprinter.Render.Text(renderStrict)

generateVerilog :: Positive -> Positive -> Positive -> Positive -> PEP2 -> Bool -> Text
generateVerilog iw ow stg dd dr int =
  (renderStrict . layoutPretty defaultLayoutOptions . fuse Shallow)
    $  header
    <> _genLinePad regs
    <> _genLinePad it
    <> _genLinePad ds
    <> _genLinePad cb
    <> _genEndModule
 where
  header   = _genHeaderVerilog (fromPositive iw) offsetOw False
  regs     = _genVerilogRegister (fromPositive stg) offsetOw (fromPositive dd)
  it       = _genIntStageVerilog (fromPositive stg) (fromPositive iw) (fromPositive ow)
  offsetOw = fromPositive ow
  ds       = _genDownSampler (fromPEP2 dr)
  cb       = _genCombStageVerilog (fromPositive stg) (fromPositive dd)
