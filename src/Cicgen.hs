-- |
-- Module      : Cicgen
-- Description : Contains types and parameter calculations
-- Copyright   : (c) Elliott Villars, 2020
-- License     : GPL-3
-- Maintainer  : elliottvillars@gmail.com
-- Stability   : experimental
-- Portability : POSIX
--
-- This module contains the functions necessary to determine the CIC filter parameters as well
-- as the types necessary to guard against undefined behavior. 
module Cicgen
  ( Positive
  , PEP2
  , toPositive
  , fromPositive
  , unsafeToPositive
  , toPEP2
  , fromPEP2
  , stageCount
  , bitGain
  , unsafeToPEP2
  )
where

import           Data.Bits

-- | Type for numbers that are greater than or equal to one.
newtype Positive = Positive Int deriving (Eq, Ord, Show, Num)

-- | Positive Exponent Power of Two.
-- Type for numbers that take the form of 2 ^ i where i is also 'Positive'.
newtype PEP2 = PEP2 Int deriving (Eq, Ord)

-- | Convert a positive integer to a 'Positive'. Returns 'Nothing' if the input is less than 1, 'Just' otherwise.
toPositive :: Int -> Maybe Positive
toPositive x = if x > 0 then Just (Positive x) else Nothing

-- | Unboxes a 'Positive' to its underlying 'Int'.
fromPositive :: Positive -> Int
fromPositive (Positive x) = x

-- | Directly constructs a 'Positive' from an 'Int' with no checking. 
unsafeToPositive :: Int -> Positive
unsafeToPositive = Positive 

-- | Checks if a 'Positive' number takes the form of 2 ^ i where i is also 'Positive'.
toPEP2 :: Int -> Maybe PEP2
toPEP2 x | x <= 1 || (popCount x /= 1) = Nothing
         | otherwise                   = Just $ PEP2 x
-- | Converts a 'PEP2' to an 'Int'
fromPEP2 :: PEP2 -> Int
fromPEP2 (PEP2 x) = x

-- | Directly constructs a 'PEP2' from an 'Int' with no checking. 
unsafeToPEP2 :: Int -> PEP2
unsafeToPEP2 = PEP2

-- |
--  Calculates the number of stages needed by the filter.
--  It is determined by dividing the target resolution by the effective number of bits AKA the conversion ratio.
stageCount
  ::
  -- | Given sampling resolution in bits.
     Positive
  ->
  -- | Conversion ratio. Must be greater than 1.
     PEP2
  ->
  -- | Resulting stage count.
     Positive
stageCount (Positive tr) (PEP2 dr) =
  Positive $ ceiling $ fromIntegral tr / (logBase 4 . fromIntegral) dr

-- |
--  Calculates the gain of the CIC filter excluding the input bit width.
--  Given by the formula: N * Log2(RM)
--  where N = number of stages, R = conversion ratio and M = differential delay.
bitGain
  ::
  -- | Stages of the CIC filter.
     Positive
  ->
  -- | Conversion ratio.
     PEP2
  ->
  -- | Differential delay of the comb sections.
     Positive
  ->
  -- | Gain in bits
     Positive
bitGain (Positive stg) (PEP2 rat) (Positive dif) = Positive res
 where
  lg  = (logBase 2 . fromIntegral) (rat * dif)
  res = ceiling $ fromIntegral stg * lg


---- | Builds the comb stage of the CIC decimator in Verilog.
--genCombDecVerilog ::
  ---- | Stages of the CIC filter
  --Positive ->
  ---- | Differential delay of each comb section.
  --Positive ->
  ---- | Generated 'Text' entity.
  --Text
--genCombDecVerilog (Positive stg) (Positive dd) = pre <> post
  --where
    --pre = "always@ (posedge i_CLK) begin\n if(r_COMB_EN) begin\n"
    --cs = map (("r_COMB_S" <>) . pack . show) [0 .. stg -1]
    --ds = map (("r_DELAY_S" <>) . pack . show) [0 .. stg -1]
    --cs' = map (("r_COMB_S" <>) . pack . show) [1 .. stg]
    --body1 = "r_DELAY_S0 <= r_COMB_S0;\n" <> body2 <> unlines [x <> " <= " <> y <> " - " <> z <> ";" | (i, x) <- indexed cs', (j, y) <- indexed cs, (k, z) <- indexed ds, i == j && j == k]
    --body2 = unlines [x <> " <= " <> y <> ";" | (i, x) <- indexed ds, (j, y) <- indexed cs, i > 0 && j > 0 && i == j]
    --post = body1 <> " o_OUT <= " <> ((<> ";\n") . ("r_COMB_S" <>) . pack . show) stg <> "end\nend\n"
--
