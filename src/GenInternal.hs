module GenInternal where
import           Prettyprinter
import           Prettyprinter.Render.Text      ( renderStrict )
import           Cicgen                         ( Positive )
import           Lexeme
import           Data.Text                      ( Text )
import qualified Data.Vector                   as V


{-| Partitions a 'Vector' based on the symmetry of its indices. |-}
_VSymSplit :: V.Vector a -> (V.Vector a, V.Vector a)
_VSymSplit v = (evens, odds)
 where
  evens = V.ifilter (\x _ -> even x) v
  odds  = V.ifilter (\x _ -> odd x) v

_genIntRegs :: Int -> V.Vector (Doc Text)
_genIntRegs x =
  ( V.zipWith (<**>) (pretty <$> V.enumFromTo 0 (2 * x))
    . V.replicate (2 * x + 1)
    )
    _genIntReg

_genCombRegs :: Int -> V.Vector (Doc Text)
_genCombRegs x =
  (V.zipWith (<**>) (pretty <$> V.enumFromTo 0 x) . V.replicate (x + 1))
    _genCombReg

_genDelayRegs :: Int -> V.Vector (Doc Text)
_genDelayRegs x = res
 where
  res = (V.zipWith (<**>) (pretty <$> V.enumFromTo 0 (x - 1)) . V.replicate x)
    _genDelayReg

_genDelayRegs' :: Int -> (V.Vector (Doc Text), V.Vector (Doc Text))
_genDelayRegs' x = (s1, s2)
 where
  res = (V.zipWith (<**>) (pretty <$> V.enumFromTo 0 (x - 1)) . V.replicate x)
    _genDelayReg
  s1 = (<> "_1") <$> res
  s2 = (<> "_2") <$> res

_genAlwaysPos :: Doc Text
_genAlwaysPos = "always@" <+> parens ("posedge" <+> _genClk) <> line

(<**>) :: Semigroup a => a -> a -> a
(<**>) x y = y <> x

_genSysRegsVerilog :: Int -> Int -> V.Vector (Doc Text)
_genSysRegsVerilog stg dd = result
 where
  result = _genIntRegs stg <> _genCombRegs stg <> dregs
  dregs  = if dd == 1 then _genDelayRegs stg else s1 <> s2
  dregs' = _genDelayRegs' stg
  s1     = fst dregs'
  s2     = snd dregs'

_genVerilogRegister :: Int -> Int -> Int -> Doc Text
_genVerilogRegister stg ow dd = (align . vsep . V.toList) result
 where
  result = (_genReg <+>) . (<> semi) <$> body
  sys    = _genSysRegsVerilog stg dd
  body'  = (_genWidthVerilog ow <+>) <$> sys
  body'' = (_genSigned <+>) <$> body'
  body   = V.cons _genCombEnable $ V.cons
    (   _genWidthVerilog ((ceiling . logBase 2 . fromIntegral) ow)
    <+> _genRatioCntr
    )
    body''

_genModName :: Doc Text
_genModName = "cic"

_genPortDir
  ::
  -- | Port direction, False is in, True is out
     Bool
  ->
  -- | Generate VHDL?
     Bool
  -> Doc Text
_genPortDir False False = "input"
_genPortDir False True  = "in"
_genPortDir True  False = "output"
_genPortDir True  True  = "out"

_genEndModule :: Doc Text
_genEndModule = _genEnd <> _genModule

_genInputTypeVerilog :: Doc Text
_genInputTypeVerilog = _genPortDir False False <+> "wire"

_genOutputTypeVerilog :: Doc Text
_genOutputTypeVerilog = _genPortDir True False <+> _genReg

_genSignExtend :: Int -> Int -> Doc Text
_genSignExtend iw bw = res
 where
  res  = braces body <> semi
  body = x <> comma <> _genIn
  x    =  braces $ pretty (bw - iw) <> braces (_genIn <> brackets (pretty iw))
-- TODO: Sign extend
_genIntStageBodyVerilog
  :: Int -> V.Vector (Doc Text) -> V.Vector (Doc Text) -> Int -> Int -> Doc Text
_genIntStageBodyVerilog stg lhs rhs iw bw = (align . vsep . V.toList) result
 where
  result = V.zipWith (<+>)
                     (_genIntRegs stg)
                     (V.cons (_genArrow <+> _genSignExtend iw bw) body')
  body' = (V.fromList . concatMap (Prelude.replicate 2) . V.toList) body
  body  = V.zipWith (\x y -> _genArrow <+> x <+> _genPlus <+> y <> semi) lhs rhs

_genIntStageVerilog :: Int -> Int -> Int -> Doc Text
_genIntStageVerilog stg iw bw = body
 where
  regs  = (_VSymSplit . _genIntRegs) stg
  evens = fst regs
  odds  = snd regs
  body  = _genAlwaysPos
    <> _genBeginBlock (_genIntStageBodyVerilog stg evens odds iw bw)

_genDownSampler :: Int -> Doc Text
_genDownSampler dr = result
 where
  result = _genAlwaysPos
    <> _genBeginBlock (_genIf (_genRatioCntr <+> "==" <+> pretty (dr - 1)) b1 b2 <> line <> b3)
  b1 = _genCombEnable <+> _genArrow <+> "1" <> semi
  b2 = _genCombEnable <+> _genArrow <+> "0" <> semi
  b3 = _genRatioCntr <+> _genArrow <+> _genRatioCntr <+> _genPlus <+> "1'b1" <> semi

-- TODO: Refactor this rat's nest
_genCombStageVerilog :: Int -> Int -> Doc Text
_genCombStageVerilog stg dd = res
 where
  cregs  = _genCombRegs stg
  cregs' = V.tail cregs
  dregs  = _genDelayRegs stg
  dregs' = _genDelayRegs' stg
  s1     = fst dregs'
  s2     = snd dregs'
  pre =
    _genCombReg
      <>  "0"
      <+> _genArrow
      <+> _genIntReg
      <>  pretty (2 * stg)
      <>  semi
      <>  line
  arrows    = V.replicate stg _genArrow
  res       = _genAlwaysPos <+> _genBeginBlock body
  body      = _genIf _genCombEnable (pre <> iclause <> out) eclause
  iclause   = (align . vsep . V.toList) (iclause1' <> iclause2')
  iclause1' = V.zipWith5 (\x y z w u -> x <+> y <+> z <+> w <+> u <> semi)
                         cregs'
                         arrows
                         cregs
                         (V.replicate stg _genMinus)
                         iclause1''
  iclause1'' = if dd == 1 then dregs else s2
  iclause2'  = if dd == 1 then b1 else b1 <> b2
  b0         = if dd == 1 then dregs else s1
  b1         = V.zipWith3 (\x y z -> x <+> y <+> z <> semi) b0 arrows cregs
  b2         = V.zipWith3 (\x y z -> x <+> y <+> z <> semi) s2 arrows s1
  eclause    = (align . vsep . V.toList) (eclause1' <> eclause2')
  eclause1'  = (\x -> x <+> _genArrow <+> x <> semi) <$> cregs
  d1         = (\x -> x <+> _genArrow <+> x <> semi) <$> dregs
  d2         = (\x -> x <+> _genArrow <+> x <> semi) <$> s1
  d3         = (\x -> x <+> _genArrow <+> x <> semi) <$> s2
  eclause2'  = if dd == 1 then d1 else d2 <> d3
  out = line <> _genOut <+> _genArrow <+> _genCombReg <> pretty stg <> semi


_genHeaderVerilog :: Int -> Int -> Bool -> Doc Text
_genHeaderVerilog iw ow sw = result
 where
  result = _genModule <+> _genModName <+> parens body <> semi
  clk    = _genInputTypeVerilog <+> _genClk
  input =
    _genInputTypeVerilog <+> _genSigned <+> _genWidthVerilog iw <+> _genIn
  output =
    _genOutputTypeVerilog
      <+> _genSigned
      <+> _genWidthVerilog ow
      <+> _genOut
      <>  line
  body = line <> (align . vsep . punctuate comma) [clk, input, output]
