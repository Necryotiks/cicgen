module Main where

import           Cicgen
import           Generate
import           Data.Maybe                     ( fromJust
                                                , isNothing
                                                )
import           Options.Applicative
import           Data.Version
import           Paths_cicgen                   ( version )
import qualified Data.Text.IO                  as T

data Arguments = Arguments
  { targetResolution  :: !Int
  , conversionRatio   :: !Int
  , inputWidth        :: !Int
  , differentialDelay :: !Int
  , integrator        :: !Bool
  , vhdl              :: !Bool
  , writeToFile       :: !Bool
  }

-- | Parser for target resolution
resParser :: Parser Int
resParser = option
  auto
  (long "target-resolution" <> short 't' <> metavar "RESOLUTION" <> help
    "Desired sampling resolution in bits,must be at least 1"
  )

-- | Parser for conversion ratio
ratioParser :: Parser Int
ratioParser = option
  auto
  (  long "conversion-ratio"
  <> short 'c'
  <> metavar "RATIO"
  <> help
       "Upsample/downsample conversion ratio, must be a positive power-of-two greater than 1"
  )

-- | Parser for input width
iWidthParser :: Parser Int
iWidthParser = option
  auto
  (long "input-width" <> short 'i' <> metavar "WIDTH" <> help
    "Input width in bits, must be at least 1"
  )

-- | Parser for differential delay
delayParser :: Parser Int
delayParser = option
  auto
  (  long "differential-delay"
  <> short 'd'
  <> metavar "DELAY"
  <> showDefault
  <> value 1
  <> help "Differential delay, must be at least 1"
  )

integParser :: Parser Bool
integParser = switch
  (long "gen-integrator" <> short 'g' <> showDefault <> help
    "Generate a CIC integrator rather than a decimator"
  )

langParser :: Parser Bool
langParser = switch
  (long "gen-vhdl" <> short 'v' <> showDefault <> help
    "Output VHDL instead of Verilog"
  )
writeParser :: Parser Bool

writeParser = switch (long "write-to-file" <> short 'w' <> showDefault <> help "Write output to a file instead of stdout")


-- | opt-parse applicative parser combinator
parseArgs :: Parser Arguments
parseArgs =
  Arguments
    <$> resParser
    <*> ratioParser
    <*> iWidthParser
    <*> delayParser
    <*> integParser
    <*> langParser
    <*> writeParser

main :: IO ()
main = run =<< customExecParser (prefs showHelpOnError) opts
 where
  versionInfo = infoOption
    (showVersion version)
    (long "version" <> short 'v' <> help "Show version number")
  opts = info
    (helper <*> versionInfo <*> parseArgs)
    (  fullDesc
    <> progDesc "Generate HDL for Cascaded-Integrator-Comb filter"
    <> header "cicgen"
    )

run :: Arguments -> IO ()
run (Arguments tr cr iw dd ig vh w) = do
  --let (tr', iw', dd') = fmap toPositive (tr,iw,dd)
  let tr' = toPositive tr
  let iw' = toPositive iw
  let dd' = toPositive dd
  let cr' = toPEP2 cr

  if Prelude.any isNothing [tr', iw', dd'] || isNothing cr'
    then fail "Input constraints unsatified, exiting"
    else do
      let tr'' = fromJust tr'
      let cr'' = fromJust cr'
      let iw'' = fromJust iw'
      let dd'' = fromJust dd'
      let stg  = stageCount tr'' cr''
      let bits = iw'' + bitGain stg cr'' dd''
      let res = generateVerilog iw'' bits stg dd'' cr'' False
      if w then T.writeFile "cic.v" res else T.putStrLn res
