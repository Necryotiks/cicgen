# CICGEN
A tool to generate a Cascaded-Intergrator-Comb filter in either Verilog or VHDL written in Haskell

## TODO
 - [ ] Finish CIC decimator in Verilog
 - [ ] Finish CIC integrator in Verilog
 - [ ] Finish CIC decimator in VHDL
 - [ ] Finish CIC integrator in VHDL
 - [ ] Write results to file
 - [x] Simplify common begin-end patterns
## Design Goals
* Generate a fully pipelined CIC filter
* Support both Verilog and VHDL
* Be human readable
* Be open source

## Installation 
 TODO

## Usage
TODO
