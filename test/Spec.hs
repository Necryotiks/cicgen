module Main where

import Cicgen
import Test.Hspec
import Test.QuickCheck
import Data.Maybe
import Data.Bits

prop_toPositiveJust :: Int -> Bool
prop_toPositiveJust x
 | x > 0 = toPositive x == Just (unsafeToPositive x)
 | otherwise = isNothing (toPositive x)

prop_fromPositiveToInt :: Int -> Property
prop_fromPositiveToInt x = x > 0 ==> fromPositive (unsafeToPositive x) == x

prop_stageCountAtLeastOne :: Int -> Int -> Property
prop_stageCountAtLeastOne x y = x > 0 && y > 1 ==> stageCount (unsafeToPositive x) (unsafeToPEP2 y) >= unsafeToPositive 1

prop_bitGainPositive :: Int -> Int -> Int -> Property
prop_bitGainPositive x y z = x > 0 && y > 0 && z > 0 ==> bitGain (unsafeToPositive x) (unsafeToPEP2 y) (unsafeToPositive z) >= unsafeToPositive 0

prop_toPEP2 :: Int -> Bool
prop_toPEP2 x = if x < 2 then isNothing (toPEP2 x) else toPEP2 x == fromBool ( x >= 1 && popCount x == 1) (unsafeToPEP2 x)

fromBool :: Bool -> a -> Maybe a
fromBool tf i = if tf then Just i else Nothing
main :: IO ()
main = hspec $ do
  describe "toPositive" $ do
    it "should check if a number is positive, returning Just if true" $ property prop_toPositiveJust
    it "should return Just if a number is positive" $ do
      toPositive 1 `shouldBe` Just (unsafeToPositive 1)
    it "should work in INT_MAX" $ do
      toPositive maxBound `shouldBe` Just (unsafeToPositive maxBound)
    it "should returns Nothing if a number is negative" $ do
      toPositive (-1) `shouldBe` Nothing
    it "should return Nothing on 0" $ do
      toPositive 0 `shouldBe` Nothing
    it "should work on INT_MIN" $ do
      toPositive minBound `shouldBe` Nothing

  describe "fromPositive" $ do
    it "should converts a Positive to an Int" $
      property prop_fromPositiveToInt
    it "should work on INT_MAX" $ do
      fromPositive (unsafeToPositive maxBound) `shouldBe` maxBound
    it "should work regardless of input value as long as type is 'Positive'" $ do
      fromPositive (unsafeToPositive (0::Int)) `shouldBe` 0
      fromPositive (unsafeToPositive (-1)) `shouldBe` -1
    it "should work on INT_MIN" $ do
      fromPositive (unsafeToPositive minBound) `shouldBe` minBound

  describe "stageCount" $ do
    it "should create at least one stage if input is valid and resolution is not zero" $ do
      property prop_stageCountAtLeastOne

  describe "bitGain" $ do
    it "should only return positive bit widths" $ do
      bitGain (unsafeToPositive 1) (unsafeToPEP2 1) (unsafeToPositive 1)  `shouldBe` unsafeToPositive 0
      bitGain (unsafeToPositive 2) (unsafeToPEP2 64) (unsafeToPositive 1) `shouldBe` unsafeToPositive 12
      bitGain (unsafeToPositive 3) (unsafeToPEP2 32) (unsafeToPositive 2) `shouldBe` unsafeToPositive 18
    it "should work for additional values" $ do
      property prop_bitGainPositive

  describe "toPEP2" $ do
   it "should work for a variety of inputs" $ property prop_toPEP2
