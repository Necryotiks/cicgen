module Main where

import Data.Vector as V
import GenInternal
  ( _VSymSplit,
    _genCombRegs,
    _genDelayRegs,
    _genIntRegs,
  )
import Test.Hspec
import Test.QuickCheck

prop_genIntRegListLen :: Int -> Property
prop_genIntRegListLen x = x > 0 ==> V.length (_genIntRegs x) == 2 * x + 1

prop_genCombRegListLen :: Int -> Property
prop_genCombRegListLen x = x > 0 ==> V.length (_genCombRegs x) == x

prop_genDelayRegListLen :: Int -> Int -> Property
prop_genDelayRegListLen x y =
  x > 0  ==> V.length (_genDelayRegs x) == x

prop_vecSplitLen :: Int -> Property
prop_vecSplitLen x =
  x
    > 0
    && even x
    ==> (V.length . fst . _VSymSplit) genVar
    == (V.length . snd . _VSymSplit) genVar
  where
    genVar = V.enumFromTo 0 (x - 1)

main :: IO ()
main = hspec $ do
  describe "_genIntRegs" $ do
    it "should return a list of Doc that whose length is equal to stages" $
      property prop_genIntRegListLen
  describe "_VSymSplit" $ do
    it "should split a vector in half if length is even" $
      property prop_vecSplitLen
    it "should return even indexed elements in the first tuple position" $ do
      (fst . _VSymSplit . V.fromList) ["A", "B", "C", "D"]
        `shouldBe` V.fromList ["A", "C"]
    it "should return odd indexed elements in the second tuple position" $ do
      (snd . _VSymSplit . V.fromList) ["A", "B", "C", "D"]
        `shouldBe` V.fromList ["B", "D"]
